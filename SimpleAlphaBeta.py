#!/usr/bin/python

import numpy as np
import random as rnd

class Player:
    def __init__(self):
        pass

    def start(self, player, train):
        self.player = player


    def next_move(self, board, reward):
        """Calculate the next move."""

        return self.maximize(board)[0]


    def maximize(self, board, alpha=float('-inf'), beta=float('inf')):

        if board.ends:
            # If there is a winner, that must be the MIN's win.
            if board.winner:
                return None, 0
            else:
                return None, 1

        moves = board.get_valid_moves()

        maxMove, maxUtility = None, alpha

        for move in moves:
            # Make a copy of the board so that we can modify it.
            boardClone = board.clone()
            boardClone.do_move(move)

            _, utility = self.minimize(boardClone, alpha, beta)

            if utility is None:
                return None, None

            if utility > maxUtility:
                maxMove, maxUtility = move, utility

            if maxUtility >= beta:
                break

            if maxUtility > alpha:
                alpha = maxUtility

        return maxMove, maxUtility


    def minimize(self, board, alpha=float('-inf'), beta=float('inf')):

        if board.ends:
            # If there is a winner, that must be the MAX's win.
            if board.winner:
                return None, 2
            else:
                return None, 1

        moves = board.get_valid_moves()

        minMove, minUtility = None, beta

        for move in moves:
            # Make a copy of the board so that we can modify it.
            boardClone = board.clone()
            boardClone.do_move(move)

            _, utility = self.maximize(boardClone, alpha, beta)

            if utility is None:
                return None, None

            if utility < minUtility:
                minMove, minUtility = move, utility

            if minUtility <= alpha:
                break

            if minUtility < beta:
                beta = minUtility

        return minMove, minUtility



    def finish(self, board, reward):
        """End the current game.
        """
        pass
