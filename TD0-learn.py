#!/usr/bin/python

import numpy as np
import random as rnd
import pickle
from Board import Board

class Player:
    def __init__(self):
        self.last_state = None
        self.alpha = 0.1
        self.explore_prob = 0.01
        self.v = {}
        self.default_value = 0.5;
        self.train = True


    def start(self, player, train):
        self.player = player

        # self.pickle_file = "save-player-{0}.p".format(player)

        # try:
        #     self.v = pickle.load(open(self.pickle_file, "rb"))
        # except:
        #     self.v = {}
        #     pickle.dump(self.v, open(self.pickle_file, "wb"))


        # Lists of states
        self.states = []
        # Whether the choice was greedy or not
        self.greedy = []

        self.train = train


    def backup(self):
        # Time-difference backup

        # do only if we are training.
        if self.train:
            for i in range(len(self.states) - 1, 0, -1):
                self.v[self.states[i-1]] += self.greedy[i] * self.alpha * (self.v[self.states[i]] - self.v[ self.states[i-1] ])


    def make_hashable(self, np_array):
        return tuple(np_array.reshape(9))


    def next_move(self, board, reward):
        """Calculate the next move."""

        moves = board.get_valid_moves()

        # Explore with probability self.explore_prob
        r = rnd.random()
        if self.train and r < self.explore_prob:
            # self.last_move_was_greedy = True

            move = rnd.choice(moves)
            self.greedy.append(False)
        else:
            # Find the move which results in the state with the state with the
            # highest value function. There may be multiple such states.
            states = []
            for m in moves:
                board_copy = board.clone()
                board_copy.do_move(m)
                states.append(board_copy)

            indices = self.highest_value_state_indices(states)
            move = moves[ rnd.choice( indices ) ]
            self.last_move_was_greedy = False
            self.greedy.append(True)


        board.do_move(move)
        self.last_state = self.make_hashable(board.board)

        self.get_value(board)   # for setting the value if the current move was random.

        # print("player: {0}, self.states = {1}, move = {2}, board = {3}".format(self.player, self.states, move, board.board))
        self.states.append(self.make_hashable(board.board))

        # print("player: {0}, self.states = {1}, move = {2}".format(self.player, self.states, move))
        return move


    def highest_value_state_indices(self, states):
        """Return the list of indices for the states with the highest value."""
        max_val = float('-inf')
        max_idx = []

        for i, state in enumerate( states ):
            s_val = self.get_value(state)
            if s_val > max_val:
                max_val = s_val
                max_idx = [i]
            elif s_val == max_val:
                max_idx.append(i)
        return max_idx


    def get_value(self, state):
        board_tuple = self.make_hashable(state.board)
        if board_tuple in self.v:
            return self.v[board_tuple]

        if state.ends:
            if state.winner == self.player:
                self.v[board_tuple] = 1.0
            elif state.winner == None: # draw
                self.v[board_tuple] = 0.5
            else:               # loser
                self.v[board_tuple] = 0.0
        else:
            self.v[board_tuple] = 0.5 # the game is in progress

        return self.v[board_tuple]


    def finish(self, board, reward):
        """End the current game.
        """

        self.v[self.last_state] = reward

        self.backup()


        rewards = {}
        for v in self.v.values():
            if v not in rewards:
                rewards[v] = 0
            rewards[v] += 1
        # print("Player: {0}, rewards={1}".format(self.player, rewards))

        # Save the policy
