#!/usr/bin/python

import numpy as np
import random as rnd
import time

from Board import Board


from Exceptions import InvalidMove

# A class for keeping track of players' scores
class PlayerScore():
    def __init__(self, obj=None, id=None, score=0):
        self.obj = obj          # The object representing the player.
        self.id = id
        self.score = score
        self.wins = 0           # Number of wins


    def reset_score(self):
        self.wins = 0
        self.score = 0


class Evaluator:
    def __init__(self, timeout=float('inf'), debug=False):
        self.start_time = None

        self.GAME_DRAW = 3      # a constant for returning draw

        self.timeout = timeout

        self.winning_score = 1
        self.draw_score = 0.5
        self.losing_score = 0

        self.debug = debug


    def set_start_time(self):
        self.start_time = time.time()


    def check_timeout(self):
        elapsed_time = time.time() - self.start_time
        if self.debug:
            print("elapsed time = ", elapsed_time)
        if elapsed_time > self.timeout:
            raise InvalidMove("Timeout!")


    def evaluate_two(self, board, player1, player2, train):
        """Play the game using two agents. The agents are expected to be the objects of
        the Player class. The winning player gets a score of 2 points. In the
        case of a draw, both players get 1 point each.
        """

        pl = [None, player1, player2] # A list to make it easier to alternate between two players.

        board.reset()


        pl[board.player].obj.start(board.player, train)
        pl[board.other_player].obj.start(board.other_player, train)

        move = None
        move_num = 0

        if self.debug:
            print("Player:", pl[1].id, "Move num: ", move_num, "Move: ", move )
            board.show()

        try:
            while True:
                self.set_start_time()
                move = pl[board.player].obj.next_move(board.clone(), 0) # reward = 0 for intermediate moves.
                self.check_timeout()

                if self.debug:
                    print("Player:", pl[board.player].id, "Move num: ", move_num, "Move: ", move )
                ends = board.do_move(move)

                move_num += 1

                if self.debug:
                    board.show()

                if ends:
                    if board.winner is None:
                        # Update the scores.
                        pl[board.player].score += self.draw_score
                        pl[board.other_player].score += self.draw_score

                        if self.debug:
                            print("Game is a draw.")

                        # Let the players know
                        pl[board.player].obj.finish(board.clone(), self.draw_score)
                        pl[board.other_player].obj.finish(board.clone(), self.draw_score)
                        break

                    else:
                        # Current player is the winner
                        pl[board.player].score += self.winning_score

                        pl[board.player].wins += 1

                        # Let the players know
                        pl[board.player].obj.finish(board.clone(), self.winning_score )
                        pl[board.other_player].obj.finish(board.clone(), self.losing_score )
                        break

        except InvalidMove as e:
            # The current player (self.player) must have made an invalid move. Penalize!
            # Give 1 point to the other player.
            pl[board.other_player].score += 1

            print(e)            # Print the Exception.


if __name__ == '__main__':
    # For the time being, assume that there are only two players and that we are given the modules of both.
    import sys, importlib.util

    spec = importlib.util.spec_from_file_location("player", sys.argv[1])
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    pl1 = PlayerScore(module.Player(), "A", 0)

    spec = importlib.util.spec_from_file_location("player", sys.argv[2])
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    pl2 = PlayerScore(module.Player(), "B", 0)

    board = Board()
    evaluator = Evaluator(2)

    print("Training.")

    for i in range(50):
        evaluator.evaluate_two(board, pl1, pl2, train=True)

        print("Game %d result: " % (i), end="")

        if board.winner is None:
            print("draw")
        else:
            print("Player %s wins." % (board.winner))

        print("Player 1 wins: {0:.2f}, Player 2 wins: {1:.2f}".format(pl1.wins/(i+1), pl2.wins/(i+1)))

    print("Final score:\n Player {0}: {1}, Player {2}: {3}".format(pl1.id, pl1.score, pl2.id, pl2.score))

    print("Testing.")

    # Reset scores
    pl1.reset_score()
    pl2.reset_score()

    for i in range(100):
        evaluator.evaluate_two(board, pl1, pl2, train=False)

        print("Game %d result: " % (i), end="")

        if board.winner is None:
            print("draw")
        else:
            print("Player %s wins." % (board.winner))

        print("Player 1 wins: {0:.2f}, Player 2 wins: {1:.2f}".format(pl1.wins/(i+1), pl2.wins/(i+1)))

    print("Final score:\n Player {0}: {1}, Player {2}: {3}".format(pl1.id, pl1.score, pl2.id, pl2.score))
