#!/usr/bin/python

import numpy as np
import random as rnd

class Player:
    def __init__(self):
        pass

    def start(self, player, train):
        self.player = player


    def next_move(self, board, reward):
        """Calculate the next move."""

        return self.maximize(board)[0]


    def maximize(self, board, alpha=float('-inf'), beta=float('inf'), depth=0):

        if board.ends:
            # If there is a winner, that must be the MIN's win.
            if board.winner:
                return None, 0
            else:
                return None, 1

        moves = board.get_valid_moves()

        maxMove, maxUtility = None, alpha

        for move in moves:
            board.do_move(move)

            _, utility = self.minimize(board, alpha, beta, depth + 1)

            if utility is None:
                board.undo_last_move()
                return None, None

            if utility > maxUtility:
                maxMove, maxUtility = move, utility

            if maxUtility >= beta:
                board.undo_last_move()
                break

            if maxUtility > alpha:
                alpha = maxUtility

            # We now have to undo the last move.
            board.undo_last_move()

        return maxMove, maxUtility


    def minimize(self, board, alpha=float('-inf'), beta=float('inf'), depth=0):

        if board.ends:
            # If there is a winner, that must be the MAX's win.
            if board.winner:
                return None, 2
            else:
                return None, 1

        moves = board.get_valid_moves()

        minMove, minUtility = None, beta

        for move in moves:
            board.do_move(move)

            _, utility = self.maximize(board, alpha, beta, depth+1)

            if utility is None:
                board.undo_last_move()
                return None, None

            if utility < minUtility:
                minMove, minUtility = move, utility

            if minUtility <= alpha:
                board.undo_last_move()
                break

            if minUtility < beta:
                beta = minUtility

            board.undo_last_move()

        return minMove, minUtility



    def finish(self, board, reward):
        """End the current game.
        """
        pass
