#!/usr/bin/python

import numpy as np

class Player:
    def __init__(self):
        pass


    def start(self, player, train):
        self.player = player


    def next_move(self, board, reward):
        """Calculate the next move."""

        return board.get_valid_moves()[0]


    def finish(self, board, reward):
        pass
