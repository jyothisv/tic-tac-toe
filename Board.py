#! /usr/bin/python

import numpy as np

from Exceptions import InvalidMove

class Board:
    def __init__(self, board_size=3, for_success=3, board=None):
        if board is None:
            self.board = np.zeros((board_size, board_size), dtype=np.uint8)
        else:
            self.board = np.array(board, dtype=np.uint8)
            # self.board[:, :] = board

        # store the board-size
        self.board_size = board_size

        self.num_empty_slots = board_size * board_size

        self.for_success = for_success

        self.GAME_DRAW = 3      # a constant for returning draw

        self.ends = False

        self.winner = None

        self.player = 1
        self.other_player = 2

        self.last_pos = []

    def clone(self):
        # Make a copy of the board.
        boardCopy = Board(self.board_size, self.for_success, self.board)
        boardCopy.num_empty_slots = self.num_empty_slots
        boardCopy.player, boardCopy.other_player = self.player, self.other_player
        return boardCopy


    def reset(self):
        # Reset the board.
        self.board[:, :] = 0

        self.player = 1
        self.other_player = 2

        self.num_empty_slots = self.board_size * self.board_size

        self.ends = False

        self.winner = None

        self.last_pos = []

    def is_legal_move(self, pos):
        """Is this move a legal move?"""
        return self.is_pos_empty(pos)

    def is_pos_empty(self, pos):
        """Is this board position empty?"""
        # print(self.board, pos, self.board[pos] == 0)
        return self.board[pos] == 0


    def do_move(self, pos):
        """Perform the move if it is legal."""
        if not self.is_legal_move(pos):
            raise InvalidMove("Illegal Move!")

        self.last_pos.append(pos)

        self.board[pos] = self.player
        self.num_empty_slots -= 1

        self.game_end_check(pos)
        if not self.ends:
            # end the turn only if the game has not finished. If the game has
            # finished, we need self.player and self.other_player's values for
            # keeping track of the score.
            self.end_turn()

        return self.ends


    def undo_last_move(self):
        """Undo the last move."""
        self.board[ self.last_pos.pop() ] = 0
        self.num_empty_slots += 1

        # If game has ended, the self.player has not changed.
        if not self.ends:
            self.undo_end_turn()

        self.ends = False
        self.winner = None


    def get_valid_moves(self):
        """Return a list of valid moves."""

        valid_moves = []
        for i in range(self.board_size):
            for j in range(self.board_size):
                pos = (i, j)
                if self.is_pos_empty(pos):
                    valid_moves.append(pos)
        return valid_moves


    def game_end_check(self, pos):
        """Check whether the game has ended.
        """
        # We know that the last move was `pos'. So we need to check only
        # whether the row/column/diagonal involving pos is uniform.

        x, y = pos
        run_row = 0
        run_col = 0
        run_rd  = 0             # Right diagonal
        run_ld  = 0             # Left diagonal

        # Row to the right
        for i in range(x, self.board_size):
            if self.board[i, y] != self.player:
                break
            run_row += 1

        # Row to the left
        for i in range(x-1, -1, -1):
            if self.board[i, y] != self.player:
                break
            run_row += 1

        if run_row >= self.for_success:
            self.ends = True
            self.winner = self.player
            return

        # Col down
        for j in range(y, self.board_size):
            if self.board[x, j] != self.player:
                break
            run_col += 1

        # Col up
        for j in range(y-1, -1, -1):
            if self.board[x, j] != self.player:
                break
            run_col += 1

        if run_col >= self.for_success:
            self.ends = True
            self.winner = self.player
            return

        # Right diagonal Up
        # x increases, y decreases
        for i, j in zip(range(x, self.board_size), range(y, -1, -1)):
            if self.board[i, j] != self.player:
                break
            run_rd += 1

        # Right diagonal Down
        # x decreases, and y increases
        for i, j in zip(range(x-1, -1, -1), range(y+1, self.board_size)):
            if self.board[i, j] != self.player:
                break
            run_rd += 1

        if run_rd >= self.for_success:
            self.ends = True
            self.winner = self.player
            return

        # Left diagonal Up
        # x decreases, y decreases
        for i, j in zip(range(x, -1, -1), range(y, -1, -1)):
            if self.board[i, j] != self.player:
                break
            run_ld += 1

        # Left diagonal Down
        # x increases, and y increases
        for i, j in zip(range(x+1, self.board_size), range(y+1, self.board_size)):
            if self.board[i, j] != self.player:
                break
            run_ld += 1

        if run_ld >= self.for_success:
            self.ends = True
            self.winner = self.player
            return

        # If we reach this point, the player has not won. Now we just check
        # whether the game is a draw.
        if self.num_empty_slots <= 0:
            self.ends = True
            self.winner = None
            return

        # Game has not ended.
        return


    def show(self):
        print(self.board)


    def __hash__(self):
        """Compute a hash of our board."""
        s = 0
        for i in np.nditer(self.board):
            s = 3*s + int(i)
        return s


    def swap_players(self):
        # Swap the players
        self.player, self.other_player = self.other_player, self.player

    def end_turn(self):
        self.swap_players()

    def undo_end_turn(self):
        """Undo an end turn. It does exactly the same thing as the end_turn function. We
        are using a different function for better clarity.
        """
        self.swap_players()


    def __eq__(self, other):
        return np.all( self.board == other.board ) and self.player == other.player
