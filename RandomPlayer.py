#!/usr/bin/python

import numpy as np
import random as rnd

class Player:
    def __init__(self):
        pass


    def start(self, player, train):
        self.player = player


    def next_move(self, board, reward):
        """Calculate the next move."""

        moves = board.get_valid_moves()


        return rnd.choice(moves)


    def finish(self, board, reward):
        """End the current game.
        """
        pass
